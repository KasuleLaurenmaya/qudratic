//KINATAAMA LAUREN
#include <stdio.h>
#include <math.h>

int main(){
    double a; double b; double c;
    printf("We are solving a Quadratic equation in the form ax^2 + bx + c = 0\n");
    printf("Enter the value of a\n");
    scanf("%lf", &a);

while(a == 0){
    printf("Error: a cannot be 0. Please enter a non-zero value for a.\n");
    printf("Enter the value of a\n");
    scanf("%lf", &a);}

    printf("Enter the value of b\n");
    scanf("%lf", &b);
    printf("Enter the value of c\n");
    scanf("%lf", &c);
    double d = pow(2, b) - (4 * a * c);

if(d < 0){
    printf("The roots are complex\n");}
else if(d >= 0){
    double e = sqrt(d);
    double x1 = (-b + e) / (2 * a);
    double x2 = (-b - e) / (2 * a);
    printf("The roots of the equation are %.4lf and %.4lf\n" ,x1 ,x2);}
    return 0;}
